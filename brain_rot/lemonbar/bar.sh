#!/usr/bin/env zsh

# Define the clock
Clock() {
        DATETIME=$(date "+%a %b %d, %r %Z")

        echo -n "$DATETIME"
}

# Define the battery
Battery() {
        BATPERC=`getbat`
        echo -n "$BATPERC"
}

while xset -q &> /dev/null; do
	echo "%{r}%{F#FFFFFF} <time: $(Clock)> <battery: $(Battery)>  "
	sleep 1
done
