# Add and commit to git
fn commit [m]{
	git add .
	git commit -m $m
}

fn put_styled [s c]{
	put (styled $s $c)
}

# Set env vars
# $paths is synced with $E:PATH
paths = [ 
	/home/skye/.local/bin
	/bin
	/sbin
	/usr/local/bin
	/home/skye/.elvish/bin
	/home/skye/.cargo/bin
	/home/skye/go/bin
	/home/skye/.ciaoroot/master/build/bin
]

E:LD_LIBRARY_PATH = $E:LD_LIBRARY_PATH

# change ls colors
E:LS_COLORS = "ex=0;31:*.flac=1;36:*.mp3=1;36:*.wav=1;36"

# Prompt stuff
color = 'cyan'
edit:prompt = { put_styled '[' $color; put $E:USER; put_styled '@' $color; put (hostname); put_styled '::' $color; put (basename (tilde-abbr $pwd)); put_styled '] % ' $color }
edit:rprompt = (constantly (styled (whoami)'@'(hostname) inverse))

# Aliases
fn ls [@a]{ e:ls --color=auto $@a }
fn la [@a]{ e:ls --color=auto -A $@a }
fn ll [@a]{ e:ls --color=auto -Al $@a }
fn cp [@a]{ e:cp -v $@a }
fn vim [@a]{ e:kak $@a }
fn push { git push origin master }
fn pull { git pull origin master }
fn startnet {
	try {
		sudo kill -9 (pgrep dhcpcd)
	} except e {
		echo "dhcpcd not running"
	}
	sudo wpa_supplicant -B -Dwext -iwlo1 -c/etc/wpa_supplicant/wpa_supplicant.conf
	sleep 5
	sudo dhcpcd
}
# this shouldn't be necesary lol
fn histfix { rm -rf /tmp/elvish-1000 }
fn killnet { sudo killall dhcpcd wpa_supplicant }

# Keybindings
edit:insert:binding[Alt-Shift-q] = { exit }
edit:insert:binding[Ctrl-l] = { clear > /dev/tty }
edit:insert:binding[Ctrl-f] = { 
	clear > /dev/tty
}
edit:insert:binding[Ctrl-h] = { edit:location:start }
edit:insert:binding[Ctrl-c] = { scrot }

# android studio fix
E:_JAVA_AWT_WM_NONREPARENTING = 1

use hay
edit:rprompt = { put_styled (date "+%H:%M:%S") $color; put_styled ' | ' white; put_styled (hay:battery) $color }
