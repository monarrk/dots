#!/usr/bin/env elvish

use hay

while (xset -q > /dev/null) {
	echo "%{r}%{F#FFFFFF} <time: "(hay:clock)"> <battery: "(hay:battery)">    "
	sleep 1
}
