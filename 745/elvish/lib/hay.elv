use str
use math

fn put_styled [s c]{
        put (styled $s $c)
}

# Center text in the terminal
fn center [line]{
	columns = (tput cols)
	printf "%*s\n" (/ (+ (count (to-string $line)) $columns) 2) $line
}

# get battery level
fn battery {
	put (getbat)
}

# well formatted clock
fn clock {
	put (date "+%a %b %d, %r %z")
}

# Display system information
fn osinfo {
        color = "magenta"

        line = (styled '[ ' $color)$E:USER(styled ' at ' $color)(hostname)(styled ' ]' $color)
	center $line
	echo "\t\tOS INFORMATION:"
	echo ''
}

fn screenshot {
	scrot '%Y-%m-%d-'(date "+%T")'_scrot.png' -e 'mv $f /home/skye/Pictures/shots/'
}
