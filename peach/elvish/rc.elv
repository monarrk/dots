# Add and commit to git
fn commit [m]{
	git add .
	git commit -m $m
}

fn put_styled [s c]{
	put (styled $s $c)
}

# Set env vars
# $paths is synced with $E:PATH
paths = [ 
	/home/skye/.local/bin
	/bin
	/usr/bin
	/sbin
	/usr/sbin
	/usr/pkg/bin
	/usr/pkgsrc/bin
	/usr/X11R7/bin
	/usr/local/bin
	/home/skye/.elvish/bin
	/home/skye/.cargo/bin
	/home/skye/.cabal/bin
	/home/skye/go/bin
	/home/skye/.ciaoroot/master/build/bin
	/home/skye/.aspen/bin
]

E:LD_LIBRARY_PATH = $E:LD_LIBRARY_PATH:/usr/pkg/lib:/usr/pkgsrc/lib:/usr/X11R7/lib

# change ls colors
E:LS_COLORS = "ex=0;31:*.flac=1;36:*.mp3=1;36:*.wav=1;36"

# Prompt stuff
color = 'magenta'
edit:prompt = { put_styled '[' $color; put $E:USER; put_styled '@' $color; put (hostname); put_styled '::' $color; put (basename (tilde-abbr $pwd)); put_styled '] % ' $color }
edit:rprompt = (constantly (styled (whoami)'@'(hostname) inverse))

# Aliases
fn ls [@a]{ e:colorls -G $@a }
fn la [@a]{ ls -A $@a }
fn ll [@a]{ ls -Al $@a }
fn cp [@a]{ e:cp -v $@a }
fn push { git push origin master }
fn pull { git pull origin master }
fn startnet {
	try {
		sudo kill -9 (pgrep dhcpcd)
	} except e {
		echo "dhcpcd not running"
	}
	sudo wpa_supplicant -B -Dbsd -iiwm0 -c/etc/wpa_supplicant.conf
}

# upload with SSH to monarrk server
fn sshup [f]{ scp $f skye@0000.directory:/home/skye/SSH }

# this shouldn't be necesary lol
fn histfix { rm -rf /tmp/elvish-1000 }
fn killnet { sudo killall dhcpcd wpa_supplicant }

# Keybindings
edit:insert:binding[Alt-Shift-q] = { exit }
edit:insert:binding[Ctrl-l] = { clear > /dev/tty }
edit:insert:binding[Ctrl-f] = { 
	clear > /dev/tty
}
edit:insert:binding[Ctrl-h] = { edit:location:start }
edit:insert:binding[Ctrl-c] = { scrot }

# android studio fix
E:_JAVA_AWT_WM_NONREPARENTING = 1

use hay
edit:rprompt = { put_styled (date "+%H:%M:%S") $color; put_styled ' | ' white; put_styled (hay:battery) $color }
