fn get_vol {
	mixerctl outputs.master | cut -d= -f2 | cut -d, -f2
}

fn set_vol [a]{
	mixerctl -w outputs.master=$a","$a
}

fn dec_vol [a]{
	current = (mixerctl outputs.master | cut -d= -f2 | cut -d, -f2)
	new = (put (- $current $a))
	mixerctl -w outputs.master=$new,$new
}

fn inc_vol [a]{
	current = (mixerctl outputs.master | cut -d= -f2 | cut -d, -f2)
	new = (put (+ $current $a))
	mixerctl -w outputs.master=$new,$new
}

fn mute {
  mixerctl -w outputs.master.mute=on
}

fn unmute {
  mixerctl -w outputs.master.mute=off
}

fn convert_mp3 [@a]{
	ls *.mp3 | each [i]{ mpg123 -w $i".wav" $i }
	rm *.mp3
}

fn burn_all_wav [@a]{
	sudo cdrecord -v dev=/dev/rcd0d -audio -pad *.wav
}

fn pkg_count {
	pkgin ls | count
}
