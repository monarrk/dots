# Monarrk's dotfiles

These are my configurations for things. They're pretty nifty in my opinion

### Screenshots
Screenshots, if I add them, can be found in `screens/`

### Info
In each directory, there should be a `date.txt` which will tell you the date in which the config was pulled in and a `uname.txt` which will tell you the operating system it is meant to run on.

