#!/bin/sh

alias cp="cp -v"

if [ -z "$1" ]; then
	NAME="`date`"
else
	NAME="$1"
fi

BASE="`pwd`"

rm -rf "$NAME"

mkdir -p "$NAME"
cd "$NAME"

date > date.txt
uname -a > uname.txt

mkdir -p sxhkd bspwm elvish lemonbar st clang-format

cp ~/.config/sxhkd/* sxhkd/
cp ~/.config/bspwm/* bspwm/
cp ~/.config/lemonbar/bar.* lemonbar/
cp -r ~/.elvish/* elvish/
cp ~/work/.clang-format clang-format/.clang-format
cp ~/Apps/st/config.h st/config.h

cd "$BASE"
mkdir -p walls

for i in `cat walls.txt`; do 
	cp "$i" walls/"`basename $i`"
done

